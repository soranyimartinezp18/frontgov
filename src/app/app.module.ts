import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModulesComponent } from './modules/modules.component';
import { HeaderComponent } from './modules/header/header.component';
import { FooterComponent } from './modules/footer/footer.component';
import { BodyComponent } from './modules/body/body.component';
import { OtroTemasComponent } from './modules/otro-temas/otro-temas.component';
import { QueremosComponent } from './modules/queremos/queremos.component';

@NgModule({
  declarations: [
    AppComponent,
    ModulesComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    OtroTemasComponent,
    QueremosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
