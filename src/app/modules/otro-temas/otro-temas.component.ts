import { Component, OnInit } from '@angular/core';
import { cards } from './card';

@Component({
  selector: 'app-otro-temas',
  templateUrl: './otro-temas.component.html',
  styleUrls: ['./otro-temas.component.css']  
})
export class OtroTemasComponent implements OnInit {
  cards = cards;
  constructor() { }

  ngOnInit(): void {
  }

}
