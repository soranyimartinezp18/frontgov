import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtroTemasComponent } from './otro-temas.component';

describe('OtroTemasComponent', () => {
  let component: OtroTemasComponent;
  let fixture: ComponentFixture<OtroTemasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtroTemasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtroTemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
