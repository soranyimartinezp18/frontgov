export const cards = [
    {
      titulo: 'Sabes qué son los datos abiertos y cómo usarlos?',
      description: 'La información que producen las entidades públicas a tu alcance',
      url: "assets/images/Otro_temas_1.JPG",
      ir: "Me interesa!"
    },
    {
        titulo: 'Conoce más sobre nuestro país',
        description: 'Colombia es hermosa, y queremos que conozcas más sobre ella, y queremos que conozcas más sobre ella',
        url: "assets/images/Otro_temas_2.JPG",
        ir: "Ir a Colombia.co"
    
    },
    {
        titulo: 'Este portal está pensado para ti',
        description: 'GOV.CO nace para facilitar a los ciudadanos la interacción con el Estado',
        url: "assets/images/Otro_temas_3.JPG",
        ir: "Quiero saber como funciona"
    },    

    {
        titulo: 'Destacado que esté en acondicionado',
        description: 'Texto descriptivo que capte la atención del lector',
        url: "assets/images/Otro_temas_4.JPG",
        ir: "Call to action"
    }
  ];
  