export const listas = [
    {
        titulo: 'Correo electrónico',
        icono: 'govco-icon-email-cn',
        url: "#"
    },
    {
        titulo: 'Regresamos tu llamada',
        icono: 'govco-icon-callback-cn',
        url: "#"
    

    },
    {
        titulo: 'Contacto telefónico ',
        icono: 'govco-icon-call-center-cn',
        url: "#"
    },
    {
        titulo: 'Llamada WEB',
        icono: 'govco-icon-callin-cn',
        url: "#"
    },
    {
        titulo: 'Redes sociales',
        icono: 'govco-icon-wifi-cn',
        url: "#"
    }
];