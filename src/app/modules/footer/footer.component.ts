import { Component, OnInit } from '@angular/core';
import { listas } from './lista';
import { listasinferiores } from './listainfe';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
listas=listas;
listasinferiores=listasinferiores;
  constructor() { }

  ngOnInit(): void {
  }

}
