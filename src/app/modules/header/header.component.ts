import { Component, OnInit } from '@angular/core';
import { items } from './menu';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  items=items;
  constructor() { }

  ngOnInit(): void {
  }

}
