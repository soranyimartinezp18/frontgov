export const listas = [
    {
      alerta: "FALTA UN DÍA",
      titulo: 'Únete al Pacto por Colombia!',
      description: 'Presidencia de la Republica',
      estilo: "alerta",
      ir: "534 colombianos participando"
    },
    {
        alerta: "ACTIVO!",
        titulo: '¿Cómo mejorarías nuestro sistema de transporte?',
        description: 'Secretaría de Movilidad de Bogotá',
        estilo: "alerta2",
        ir: "87 colombianos participando"
    
    },
    {
        alerta: "CONOCE LOS RESULTADOS",
        titulo: 'Los datos y visualizaciones del gobierno interesantes para su uso, aprovechamiento y toma de decisiones.',
        description: 'Ministerio de las Tecnologías de la Información y las Comunicaciones',
        estilo: "alerta3",
        ir: ""
    },    
  ];
  