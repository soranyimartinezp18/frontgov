import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueremosComponent } from './queremos.component';

describe('QueremosComponent', () => {
  let component: QueremosComponent;
  let fixture: ComponentFixture<QueremosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QueremosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QueremosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
